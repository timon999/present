from markdown import markdown
from sys import argv
from jinja2 import Template, escape
from re import search

def escapeumlauts(s):
    u = {
        u'\xe4': "&auml;",
        u'\xc4': "&Auml;",
        u'\xf6': "&ouml;",
        u'\xd6': "&Ouml;",
        u'\xfc': "&uuml;",
        u'\xdc': "&Uuml;",
    }
    return "".join([u[c] if c in u else c for c in s.decode("utf-8")])

def grep(p, s):
    return "\n".join([line for line in s.split("\n") if search(p, line)])

def grepo(p, s):
    return "\n".join(
        [search(p, l).groups()[0] for l in s.split("\n") if search(p, l)])

with open("present_templ.html", "r") as f:
    HTML = f.read()

with open(argv[1]) as f:
    source = f.read()

source = grep(r"^(?!--)", source)
source = source.replace("&amp;", "&")
options = grepo(r":(.+)", source).split("\n")
source = grep(r"^([^:]|:$|$)", source)
source = str(escape(escapeumlauts(source)))
slides = source.split("\n:\n")
slides = [markdown(slide) for slide in slides]
slides = [slide.replace("\n", "") for slide in slides]
slides = [slide.replace('"', r'\"') for slide in slides]
template = Template(HTML)
config = {
    "slides": slides,
    "size": "120px",
    "font": "Consolas,monospace,sans-serif",
    "bg_color": "#FFFFFF",
    "color": "#000000",
    "pagenumber": False,
}
config.update({l.split(" ")[0]:l.split(" ",1)[1] for l in options})

html = template.render(config)

with open(argv[2], "w") as f:
    f.write(html)
