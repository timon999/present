# -*- coding: UTF-8 -*-

import sys
from jinja2 import Template, escape


with open("present_templ.html", "r") as f:
    HTML = f.read()


def escapeumlauts(s):
    u = {
        u'\xe4': "&auml;",
        u'\xc4': "&Auml;",
        u'\xf6': "&ouml;",
        u'\xd6': "&Ouml;",
        u'\xfc': "&uuml;",
        u'\xdc': "&Uuml;",
    }
    return "".join([u[c] if c in u else c for c in s.decode("utf-8")])


def command(line):
    if line == "@":
        return "&nbsp;"
    cmds = {
        "img": lambda i: "<img src=\'" + i + "\'/>",
        "l": lambda: "<ul>",
        "/l": lambda: "</ul>",
        "-": lambda *line: "<li>" + " ".join(line) + "</li>",
        "u": lambda *line: "<u>" + " ".join(line) + "</u>",
        "b": lambda *line: "<b>" + " ".join(line) + "</b>",
    }
    cmd = line.split(" ")[0][1:]
    return cmds[cmd](*line.split(" ")[1:])


def convert(text):
    options = {}
    text = escapeumlauts(text)
    for line in text.split("\n"):
        if line.startswith(":"):
            option = line.split(" ")[0][1:]
            value = " ".join(line.split(" ")[1:])
            options[option] = value
    text = str(escape(text))
    text = text.replace("&amp;", "&")
    t_lines = text.split("\n")
    t_lines = [l for l in t_lines if not l.startswith(":")]
    t_lines = [command(l) if l.startswith("@") else l for l in t_lines]
    text = "\n".join(t_lines)
    slides = text.split("\n\n")
    slides = [slide.replace("\n", "<br>") for slide in slides]
    return options, slides


def main():
    filename = sys.argv[1]
    with open(filename, "r") as f:
        text = f.read()
    template = Template(HTML)
    options, slides = convert(text)
    config = {
        "slides": slides,
        "size": "120px",
        "font": "Consolas,monospace,sans-serif",
        "bg_color": "#FFFFFF",
        "color": "#000000",
    }
    config.update(options)
    with open(sys.argv[2], "w") as f:
        f.write(template.render(config))

if __name__ == "__main__":
    main()
